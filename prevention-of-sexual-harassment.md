# **Prevention of Sexual Harassment**

## **What kinds of behaviour cause sexual harassment?**

There are three kinds of behavior that is,

- **Verbal:** Verbal harassment occurs like,
    - comments about clothing.
  
    - a person's body.
  
    - sexual or gender-based jokes or remarks.
  
    - also include- sexual innuendos, threads, and spreading rumors about a person's personal life.

- **Visual:** Visual harassment occurs like,
    - Obscene pictures or drawings
  
    - Posters

    - Screensavers

    - Cartoons

    - Emails or texts of sexual nature

- **Physical**: Physical harassment occurs like,
    - Sexual assaults impending,
  
    - Impending and blocking movements.
  
    - Inappropriate touching such as kissing, hugging, patting, stroking, rubbing, etc.

## **What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

If I face or witness any incident or repeated incidents of harmful behavior, there are several things I can do:

- Stay safe: If I or someone else is in immediate danger, call the police or emergency services right away.

- Speak up: If I feel comfortable doing so, let the person know that their behavior is not acceptable. Sometimes, people may not realize that their behavior is harmful, and a conversation can help them understand the impact of their actions.

- Document the incident: Write down the details of the incident, including the date, time, location, and names of any witnesses. Keep any relevant evidence, such as emails or messages.

- Seek support: If I am feeling overwhelmed or distressed, seek support from a trusted friend, family member, or a mental health professional.

- Report the incident: If I am in a workplace, school, or any other organization, report the incident to the appropriate authority or supervisor. Many organizations have policies in place to address such incidents and will take steps to investigate and address them.

- Advocate for change: If I witness repeated incidents of harmful behavior, advocate for change by raising awareness and supporting policies or initiatives to prevent such behavior.

I will also remember and tell the victim, it is not his/her fault if someone behaves in a harmful or abusive way towards you, and you have the right to take action to protect yoursef.