# **Energy Management**

## **Manage Energy not Time**

### **Question 1:**

What are the activities you do that make you relax - Calm quadrant?

- go for a walk
- listen soft music.
- watch a movie.
- have a conversation with family and friends.

### **Question 2:**

When do you find getting into the Stress quadrant?

- get stuck in solving problems.
- have less time to complete tasks before deadlines.

### **Question 3:**

How do you understand if you are in the Excitement quadrant?

- when I finish the task before the deadline.
- planning for a trip.
- when someone tells some good news.

## **Sleep is your superpower**

### **Question 4**

Paraphrase the Sleep is your Superpower video in detail.

- sleep is very essential for our brain. After a quality sleep, our brain absorbs more information if we try to learn something.
- as adult age, we sleep a minimum of 7 hours per day to work more effectively.
- lack of deep-quality sleep can cause rapid aging.
- lack of sleep can cause immune deficiency. One can lose 75% of killer cells which tend to fight the germs and viruses inside our body.
- go to bed at the same time every day. Stay consistent with your bedtime.

### **Question 5**

What are some ideas that you can implement to sleep better?

- have a fixed timetable, sleep and wake up at the same time every day.
- do not use electronic devices before and after sleep.
- listen to white noise if you're struggling to fall asleep.

### **Question 6**

Paraphrase the video - Brain Changing Benefits of Exercise. 

- exercise leads to better mood, better energy, better focus, and better attention
- exercise makes our pre-frontal cortex more immune to cognitive diseases.
- studies have shown that exercise can increase the volume of the hippocampus.
- exercise changes the brain's anatomy, physiology, and function.
- exercise leads to the production of brand-new brain cells that increase long-term memory.

### **Question 7**

What are some steps you can take to exercise more?

- use of stairs, whenever possible.
- avoiding transport and walking if feasible
- improve your quality of sleep.
- indulge in any form of physical activity whenever possible