# **Grit and Growth Mindset**

## **1. Grit**

### **Paraphrase (summarize) the video in a few lines. Use your own words.**

It is found in research that grit is the driving force for any individual toward learning, to learn anything you need the courage to be on that path which is not easy to pursue, and for that courage, you need to be dedicated toward growth mindset. Irrespective of individual skill set, IQ, and other factors, if anyone wants to learn he just needs to be keen to learn and should have strong willpower. And this is all about personality development and having a growth mindset.

### **What are your key takeaways from the video to take action on?**

- Try harder next time. Avoid giving up when you fail.
- A growth mindset is of utmost importance.
- Grit is sticking with your future, day in, day out, not just for the week, not just for the month, but for years, and working really hard to make that future a reality. Grit is living life like it's a marathon, not a sprint.
  
## **2. Introduction to Growth Mindset**

### **Paraphrase (summarize) the video in a few lines in your own words.**

The video mainly shows the difference between a Fixed Mindset and a Growth Mindset.
  
- A growth mindset is a belief that one's abilities and intelligence can be developed through effort and learning. This mindset is characterized by a focus on improvement and a willingness to try new things, even in the face of failure.
  
- On the other hand, a fixed mindset is the belief that one's abilities and intelligence are fixed and cannot be changed. This mindset is characterized by a fear of failure and a reluctance to try new things.
  
- A growth mindset is important because it allows people to take on challenges and learn from their mistakes, rather than being discouraged by them.
  
- A fixed mindset, on the other hand, can hold people back and prevent them from reaching their full potential.

### **What are your key takeaways from the video to take action on?**

- Able to differentiate between growth and fixed mindset.

- Instead of viewing mistakes as failures, I'll try to view them as opportunities to learn and grow. This will help me develop a growth mindset.

- I'll seek feedback from my mentor on my performance and use it to identify areas for improvement, which will eventually help me in achieving a growth mindset.

## **3. Understanding Internal Locus of Control**

### **What is the Internal Locus of Control? What is the key point in the video?**

- Internal locus of control is the key to staying motivated.

- Build up the belief that you are in control of your destiny, that you have an internal locus of control, and you will never have issues with motivation in your life.

The three best ways to be followed to adopt Internal Locus of Control and to start being motivated are:

- Solving Problems in one’s Life
- Taking Time to appreciate one’s actions
- Noticing improvements and efforts taken by an individual

## **4. How to build a Growth Mindset**

### **Paraphrase (summarize) the video in a few lines in your own words.**

- One needs to believe in one self's ability to figure things out on their own.
- A growth mentality is based on the notion that everything is possible.
- It is important to challenge the assumption we make about our abilities. we should not assume that we cannot enhance our abilities.
- Avoid discouragement when we fail and honor the failure to be just part of life learn from it and move on.

### **What are your key takeaways from the video to take action on?**

A growth mindset means that we thrive on challenges, and don’t see failure as a way to describe ourselves but as a springboard for growth and developing our abilities. Our intelligence and talents are all susceptible to growth.

To develop a Growth mindset:

- Believe in our ability to figure things out.
- Question our negative assumptions.
- Create our own curriculum for skill development.
- Honor the struggle.

## **4. Mindset - A MountBlue Warrior Reference Manual**

### **What are one or more points that you want to take action on from the manual?**

- I will stay relaxed and focused no matter what happens.

- I will follow the steps required to solve problems:
    - Relax
    - Focus - What is the problem? What do I need to know to solve it?
    - Understand - Documentation, Google, Stack Overflow, Github Issues, Internet
    - Code
    - Repeat
