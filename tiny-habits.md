# **Tiny Habits**

## **1. Tiny Habits - BJ Fogg**

**Your takeaways from the video**
- Behaviour change is systematic
- Changing our environment is one way of changing our behaviour
- Motivation works only for short-term to change our habits
- The effective way to change our behaviour is to make our behaviour change tiny enough
- We can start with easy to do things which are independent of motivation and progressively move towards the difficult ones

## **2. Tiny Habits by BJ Fogg - Core Message**

**Your takeaways from the video in as much detail as possible**
- The formula for behaviour is Motivation, Ability and Prompt.
  
- Easy to Do things require low motivation and vice-versa.
  
- Adopting a habit is easy but being consistent with it hard.
  
- The solution is to shrink the habit to it’s tiniest form. This way it becomes easy to adopt and make it permanent.
  
- A habit can be shrunk to it’s tiniest form by either reducing the quantity or doing the first step.
  
- There are three prompt which trigger the habit.
  - External prompt (Phone notification).
  - Internal prompt (thoughts and emotions)
  - Action prompt (chaining of a tiny behaviour with an existing behaviour)

- The action prompt is the best way to adopt a new habit. This prompt leverages the momentum we already have to a new small thing.
  
- Learning to celebrate after accomplishment of a work is the most critical component of habit development. It keeps the motivation up for doing the habit next time.
  
- When we accomplish something. Our confidence and motivation level ups. This is the success moment which gets created by the frequency of successes but not on the size of the success.

**How can you use B = MAP to make making new habits easier?**

- Just after waking up, I’ll make my bed.
- After having my dinner, I’ll go for a small walk.
- When we are going for any big task we can use this rule that make us more comfortable with any kind of condition.
- After hitting the bed, I’ll think of one good thing that happened that day.

**Why it is important to "Shine" or Celebrate after each successful completion of habit?**

In order to keep one motivated and pumped up, one should celebrate the successful completion of tasks. As this small celebration keeps the person motivated and encourages him to add up some complexity to the task.

## **3. 1% Better Every Day Video**

**Your takeaways from the video**
- The aggregation of marginal gains is huge over time
- Good habits make time our friend whereas bad habits make them our enemy
- Motivation does not help in the long run to achieve something
- Clarity is more important than motivation for being better at something
- Our environment plays a role in shaping our habits
- Quantity is more important than quality for developing habits
- We should make sure to not break the chain of our good habits so that - we get motivation from the progress

## **4. Book Summary of Atomic Habits**

**Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?**
- An atomic habit is a regular practice or routine that is not only small and easy to do but is also the source of incredible power; a component of the system of compound growth.

- Bad habits repeat themselves again and again not because you don’t want to change, but because you have the wrong system for change.

- Changes that seem small and unimportant at first will compound into remarkable results if you’re willing to stick with them for years.

**Write about the book's perspective on how to make a good habit easier?**
- Make it obvious, make it easy to trigger
- Keep fewer steps between you and the good behaviour
- Make it immediately satisfying

**Write about the book's perspective on making a bad habit more difficult?**

- We can make that habit non-obvious i.e we can change our environment to avoid those bad habits
- We can make those habits unattractive by attaching boring activities to it
- We can make it difficult by increasing the friction to do that activity
- We can make it unsatisfying by using a punishment mechanism

## **5. Reflection:**

**Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

- I will adopt the habit of eating healthy only from now on.
- I will create my identity as a healthy person. I will not try to eat junks everyday. May be once or twice a month.

**Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

Sleeping late
- I will keep all the devices and distractions away from the bed